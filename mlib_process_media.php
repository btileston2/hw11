<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("Process Media");
require('mlib_header.php');
require('mlib_sidebar.php');

# Code for your web page follows.
echo "<h2>Processing Media from List...</h2>";

if ($_FILES['userfile']['error'] > 0)
{
  echo 'Problem: ';
  switch ($_FILES['userfile']['error'])
  {
    case 1:  echo 'File exceeded upload_max_filesize'; break;
    case 2:  echo 'File exceeded max_file_size'; break;
    case 3:  echo 'File only partially uploaded'; break;
    case 4:  echo 'No file uploaded'; break;
  }
  exit;
}

//does follow right MIME type?
if ($_FILES['userfile']['type'] != 'text/plain')
{
  echo 'Problem: file is not plain text';
  exit;
}

//put file wher we want it
$upfile = './uploads/'.$_FILES['userfile']['name'];

if (is_uploaded_file($_FILES['userfile']['tmp_name']))
{
  if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $upfile))
  {
    echo 'Problem: Could not move file to destination directory';
    exit;
  }
}
else
{
  echo 'Problem: Possible file upload attack. Filename: ';
  echo $_FILES['userfile']['name'];
  exit;
}

echo 'File uploaded successfully<br><br>';

//display file content
$fp = fopen($upfile, 'r');

if (!$fp)
{
  echo "<p>I could not open $upfile right now.";
  exit;
}

//read data line by line.
while (!feof($fp))
{
  $line = fgets($fp);
  $line_array = explode(',', $line);
  $title = trim($line_array[0]);
  $author = trim($line_array[1]);
  $type = trim($line_array[2]);
  $description = trim($line_array[3]);

  //validate
  $errors = validate_media($title, $author, $type, $description);
  if (empty($errors)) {
    //display
    echo "Title: $title <br/>";
    echo "Author: $author <br/>";
    echo "Type: $type <br/>";
    echo "Description: $description <br/>";

    //insert into database
    try {
      //open db
      $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $db->exec("INSERT INTO media (title, author, description, type, user_id) VALUES ('$title', '$author', '$description', '$type', 0);");
      //close
      $db = NULL;
    }
    catch(PDOException $e){
      echo 'Exception : '.$e->getMessage();
      echo "<br/>";
      $db = NULL;
    }
  } else {
    echo "Errors found in Media entry:<br/>";
    echo "Title: $title <br/>";
    echo "Author: $author <br/>";
    echo "Type: $type <br/>";
    echo "Description: $description <br/>";
    foreach($errors as $error) {
      echo $error."<br/>";
    }
  }
  echo "<br/>";
}

//close file
fclose($fp);
require('mlib_footer.php');
?>
