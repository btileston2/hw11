<?php
require('mlib_values.php');
# CSCI59P standard functions
function html_head($title) {
  echo '<html lang="en">';
  echo '<head>';
  echo '<meta charset="utf-8">';
  echo "<title>$title</title>";
  echo '<link rel="stylesheet" href="mlib.css">';
  echo '</head>';
  echo '<body>';
}

function try_again($str) {
  echo $str;
  echo "<br/>";
  //back button
  echo '<a href="#" onclick="history.back(); return false;">Try Again</a>';
  require('mlib_footer.php');
  exit;
}

function validate_media($title,$author,$type,$description) {
  $error_messages = array(); #create empty mssge arry
  if ( strlen($title) == 0 ) {
    array_push($error_messages, "Media must have a title.");
  }

  if ( strlen($author) == 0 ) {
    array_push($error_messages, "Media must have an author.");
  }
  if ( strlen($type) == 0 ) {
    array_push($error_messages, "Type field must have a media type.");
  }

  if ( strlen($description) == 0 ) {
    array_push($error_messages, "Media must have a description.");
  }

  try {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //check invalid media type
    if ( strlen($type) != 0 ) {
      $sql = "SELECT COUNT(*) FROM mlib_types where type = '$type' AND status = 'active'";
      $result = $db->query($sql)->fetch(); //count num entries with tool name
      if ( $result[0] == 0) {
        array_push($error_messages, "Media type $type is not defined. Type must be valid.");
      }
    }

    //chck dup title name
    if ( strlen($title) != 0 ) {
      $sql = "SELECT COUNT(*) FROM media where title = '$title' AND status = 'active'";
      $result = $db->query($sql)->fetch(); //count num entries with tool name
      if ( $result[0] > 0) {
        array_push($error_messages, "$title is not unique. Titles must be unique.");
      }
    }
  }

  catch(PDOException $e){
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }

  return $error_messages;
}


//validate date format as yyyy-mm-dd
function MyCheckDate( $postedDate ) {
   if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $postedDate, $datebit)) {
      return checkdate($datebit[2], $datebit[3], $datebit[1]);
   } else {
      return false;
   }
}
?>
