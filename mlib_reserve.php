<!doctype html>
<?php
require('mlib_values.php');
require('mlib_functions.php');
html_head("mlib reserve");
require('mlib_header.php');
require('mlib_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //get date two week in advance
    $result = $db->query("SELECT CURDATE() + INTERVAL 1 WEEK")->fetch();
    $next_week = $result[0];
?>
  <!-- Display a form to capture information -->
  <h2>Reserve Equipment</h2>
  <form action="mlib_reserve.php" method="post">
    Checked out to:
    <select name="user">
<?php
    //display all users in utable
    $result = $db->query('SELECT * FROM mlib_users');
    foreach($result as $row)
    {
      print "<option value=".$row['id'].">".$row['first']."</option>";
    }
?>
    </select><br/>
    Reserve Till (yyyy-mm-dd):
    <?php print "<input type 'text' name='date_in' value='$next_week' /><br/>"; ?>
    <table border=1>
      <tr>
        <td>Click to Reserve</td><td>Title</td><td>Author</td><td>Description</td><td>Type</td>
      </tr>

     <?php
         $result = $db->query("SELECT * FROM media WHERE status = 'active' and user_id = 0 ORDER by type");
         foreach($result as $row)
         {
           print "<tr>";
           print "<td><input type='checkbox' name='id[]' value=".$row['id']."></td>";
           print "<td>".$row['title']."</td>";
           print "<td>".$row['author']."</td>";
           print "<td>".$row['description']."</td>";
           print "<td>".$row['type']."</td>";
	 }
?>
    </table>
    <input type="submit" name="submit" value = "Submit"/><br/>
  </form>

<?php

    //close db
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }


} else {
?>

    <h2>Equipment Reserved</h2>

<?php
  # Process the information from the form displayed
  $id = $_POST['id'];
  $user = $_POST['user'];
  $date_in = $_POST['date_in'];

  try
  {
    //open db
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //get name for the user
    $result = $db->query("SELECT * FROM mlib_users where id = $user")->fetch();
    $user_name = $result['first']." ".$result['last'];

    //get today date
    $result = $db->query("SELECT CURDATE()")->fetch();
    $today = $result[0];

    //validate date_in
    if (!MyCheckDate($date_in)) {
      try_again("Invalid date, Format yyyy-mm-dd");
    }

    //check for date in past
    if ($date_in < $today) {
      try_again("The Reserve Till date is earlier than today: ".$today);
    }


    $n = count($id);
    if ($n == 0) {
      echo "You did not select any items to reserve.<br/>";
    } else {
      //update each piece of equipment with user_id, date in
     for($i=0; $i < $n; $i++)
     {
       $db->exec("UPDATE media SET user_id = $user, date_in = '$date_in' WHERE id = $id[$i]");
     }

     //now output
     print "<table border=1>";
     print "<tr>";
     print "<td>Title</td><td>Author</td><td>Description</td><td>Type</td><td>User</td><td>Reserved Till</td>";
     print "</tr>";
     for($i=0; $i < $n; $i++)
     {
       $sql = "SELECT * FROM media where id = $id[$i]";
       $row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
       print "<tr>";
       print "<td>".$row['title']."</td>";
       print "<td>".$row['author']."</td>";
       print "<td>".$row['description']."</td>";
       print "<td>".$row['type']."</td>";
       print "<td>".$user_name."</td>";
       print "<td>".$row['date_in']."</td>";
       print "</tr>";
     }
     print "</table>";
   }

   //close db
   $db = NULL;
 }
 catch(PDOException $e)
 {
   echo 'Exception : '.$e->getMessage().'<br/>';
   $db = NULL;
 }

}
require('mlib_footer.php');
?>
