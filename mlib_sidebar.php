<aside id="sidebar">
  <nav>
    <h2>Actions</h2>
    <ul>
      <li>
        <a href="mlib_status.php">Media Status</a><br/>
      </li>
      <li>
        <a href="mlib_media.php">Add Media</a><br/>
      </li>
      <li>
        <a href="mlib_upload.php">Upload Media</a><br/>
      </li>
      <li>
        <a href="mlib_reserve.php">Reserve Media</a><br/>
      </li>
      <li>
        <a href="mlib_release.php">Release Media</a><br/>
      </li>

    </ul>
    <hr/>
  </nav>
</aside>
